from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG('sleep', default_args=default_args, schedule_interval=timedelta(days=1), catchup=False)


t1 = BashOperator(
    task_id='sleep',
    bash_command='sleep 600',
    dag=dag)