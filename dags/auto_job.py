from airflow import DAG
from datetime import datetime, timedelta
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 9, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    # 'retry_delay': timedelta(minutes=5)
}

dag = DAG('auto_job', default_args=default_args, schedule_interval=None, catchup=False)

# {{ dag_run.conf["image"] if dag_run.conf["image"] else "registry.sofi.com/automation-jobs:master" }}
auto_job = KubernetesPodOperator(namespace='airflow',
                          image="{{ dag_run.conf['image'] }}",
                          env_vars={"AUTOMATION_JOB_NAME": "TEST_AUTOMATION_JOB", "AUTOMATION_JOB_FILE_STEP": "0", "AUTOMATION_JOB_FILE_ID": "{{ dag_run.conf['file_id'] }}", "AUTOMATION_JOB_FILE_NAME": "{{ dag_run.conf['file_name'] }}"},
                          name="auto-test-job",
                          task_id="auto-test-job",
                          get_logs=True,
                          in_cluster=True,
                          is_delete_operator_pod=True,
                          dag=dag
                          )