from airflow import DAG
from datetime import datetime, timedelta
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 10, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    # 'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'kubernetes_sample', default_args=default_args, schedule_interval=timedelta(days=1), catchup=False)


hello_world = KubernetesPodOperator(namespace='airflow',
                          image="hello-world",
                          labels={"foo": "bar"},
                          name="hello-world",
                          task_id="hello-world",
                          in_cluster=True,
                          get_logs=True,
                          dag=dag
                          )