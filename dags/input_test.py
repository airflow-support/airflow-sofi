from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 11, 1),
    'email': ['airflow@example.com'],
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'schedule_interval': None,
}

dag = DAG('input_test', default_args=default_args, schedule_interval=timedelta(days=1), catchup=False)


t1 = BashOperator(
    task_id='input_test',
    bash_command='echo "Airflow Input Parametes: \'{{ dag_run.conf["message"] if dag_run.conf["message"] else "Git gud you loser" }}\'"',
    dag=dag)

t2 = BashOperator(
    task_id='env_test',
    bash_command="env",
    dag=dag)

t2.set_upstream(t1)