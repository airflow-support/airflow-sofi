#!/bin/sh -ex
VERSION=0.7.3
PKG=envconsul_${VERSION}_linux_amd64.tgz \
SRC=https://releases.hashicorp.com/envconsul \
CKSUM=fee1281654a224337a573ac135324fadcd88b19128db424aa5f5562b1236e314

curl -sLO ${SRC}/${VERSION}/${PKG}
echo "${CKSUM}  ${PKG}" > ${PKG}.SHA256
sha256sum -c ${PKG}.SHA256
tar -xzf ${PKG} -C /usr/sbin/
rm ${PKG} ${PKG}.SHA256