max_stale = "10m"
sanitize  = true
upcase    = true

exec {
  kill_timeout = "5s"
  splay = "5s"
}

kill_signal = "SIGTERM"

consul {
  retry {
    enabled = true
    attempts = 3
    backoff = "5s"
    max_backoff = "30s"
  }
}

vault {
  renew_token = false
  retry {
    enabled = true
    attempts = 3
    backoff = "5s"
    max_backoff = "30s"
  }
}

secret {
  path = "secret/db-owner/airflow"
}

secret{
  path = "secret/airflow"
}

prefix {
  path = "config/db/airflow"
  format = "db_airflow_{{ key }}"
}

prefix {
  path = "config/airflow"
  format = "airflow_{{ key }}"
}

prefix {
  path = "config/global"
  format = "global_{{ key }}"
}