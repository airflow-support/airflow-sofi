#!/usr/bin/env bash

echo "Running the sofi airflow entrypoint..."
# Using vault / envconsul to supply environment.
if [ -n "${VAULT_ADDR}" ]; then
    exec envconsul -consul $NODE_IP:8500 -config /config.hcl -once /start "$@"
else
    : "${DB_AIRFLOW_HOST:="postgres11.default"}"
    : "${DB_AIRFLOW_PORT:="5432"}"
    : "${SECRET_DB_OWNER_AIRFLOW_USERNAME:="airflow"}"
    : "${SECRET_DB_OWNER_AIRFLOW_PASSWORD:="sofi"}"
    : "${DB_AIRFLOW_DBNAME:="sofi_airflow"}"

    # Defaults and back-compat
    : "${AIRFLOW_HOME:="/usr/local/airflow"}"
    exec /start
fi

